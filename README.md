# RAKBUKU APP 

### [Live Demo](https://nfathan.github.io/rakbuku/) 

# Overview

This project demostrates CRUD Application in React.

# To run the Project

Run the following command to start application in development mode

### `npm install`

install dependencies

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
