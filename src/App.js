// Routing
import { 
  Routes, 
  Route,
} from "react-router-dom";
// Pages
import AddBook from './pages/AddBook';
import EditBook from './pages/EditBook';
import Home from "./pages/Home";
// Components
import AppHeader from './components/AppHeader';
import Footer from './components/Footer';
import NotFound from './components/NotFound';
import Modal from "./components/Modal";
// Styles
import './App.scss';

function App() {

  return (
    <>
      <AppHeader />
      <main>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/addbook" element={<AddBook />} />
          <Route path="/editbook/:bookId" element={<EditBook />} />
          <Route path="/*" element={<NotFound />} />
        </Routes>
        <Modal />
      </main>
      <Footer />
    </>
  );
}

export default App;
