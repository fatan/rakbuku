import React from 'react';
import ReactDOM from 'react-dom';
// Routing
import { 
  // BrowserRouter, 
  HashRouter 
} from 'react-router-dom'
// App
import App from './App';
// Context
import { AppProvider } from './context';
// Styles
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <HashRouter>
      <AppProvider>
        <App />
      </AppProvider>
    </HashRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
