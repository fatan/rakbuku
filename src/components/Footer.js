function Footer() {
  return (
    <footer className="center">
      <p>Design and Coded by <a href="https://nfathan.github.io/fathan-nasrullah/">Fathan Nasrullah</a></p>
    </footer>
  )
}

export default Footer 