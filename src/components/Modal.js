import { useCallback, useEffect } from "react"
import { useGlobalContext } from "../context"

function Modal() {
  const { books, display, setDisplay } = useGlobalContext()

  const handleModal = useCallback(() => {
    setTimeout(() => {
      setDisplay(false)
    }, 10000)
  }, [setDisplay]) 

  useEffect(() => {
    handleModal()
  }, [display, handleModal])

  return (
    <>
      {display && 
        <div className="Modal">
          <p>Buku {books[books.length - 1].title} berhasil ditambahkan ke dalam Rak Buku</p>
        </div>
      }
    </>
  )
}

export default Modal 