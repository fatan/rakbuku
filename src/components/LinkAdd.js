import { Link } from 'react-router-dom'

function LinkAdd(props) {
  return (
    <Link 
      to={props.to} 
      className={props.className}>
        <props.icon />
        {props.innerText}
    </Link>
  )
}

export default LinkAdd