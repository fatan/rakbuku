// Icons
import { TiPlus } from 'react-icons/ti'
// Components
import AppLogo from './AppLogo'
import LinkAdd from './LinkAdd'

function AppHeader() {
  return (
    <header className="AppHeader-wrapper">
      <div className="container">
        <div className="AppHeader flex jcsb">
          <div className="AppHeader-logo">
            <AppLogo />
          </div>
          <div className="AppHeader-menu">
            <div className="LinkTo">
              <LinkAdd
                to="/addbook" 
                className="add flex flex-d-column" 
                icon={TiPlus}
                innerText="Tambah Buku"
              />
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default AppHeader