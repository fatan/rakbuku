// Icons
import {
  MdOutlineEdit,
  MdDeleteForever,
  MdDoneAll,
  MdRemoveDone 
} from 'react-icons/md'

function Book(props) {
  const { id, title, author, year, image, isComplete } = props

  return (
    <li className="Book shadow">
      <div className="Book-img">
        <img src={image} alt={title} />
      </div>
      <div className="Book-header">
        <h3>{title}</h3>
      </div>
      <div className="Book-info">
        <p>Penulis : {author}</p>
        <p>Tahun : {year}</p>
      </div>
      <div className="Book-button-wrapper center mt-10">
        {isComplete ? 
          <button 
            className="complete-button" 
            onClick={() => props.toggleIsComplete(id, isComplete)}>
              <MdRemoveDone /> Belum Selesai
          </button>
          : 
          <button 
            className="complete-button" 
            onClick={() => props.toggleIsComplete(id, isComplete)}>
              <MdDoneAll /> Selesai
          </button> 
        }
        <button 
          className="edit-button"
          onClick={() => props.editBook(id, title, author, year, image, isComplete)}>
            <MdOutlineEdit />
        </button>
        <button 
          className="delete-button"
          onClick={() => props.deleteBook(id, title)}>
            <MdDeleteForever />
        </button>
      </div>
    </li>
  )
}

export default Book 