import { Link } from 'react-router-dom'

function AppLogo() {
  return (
    <div className="App-logo">
      <h1><Link to="/">RAKBUKU</Link></h1> 
    </div>
  )
}

export default AppLogo