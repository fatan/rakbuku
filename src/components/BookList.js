import { useNavigate } from 'react-router'
// Context
import { useGlobalContext } from '../context'
// Components
import BookListCompleted from '../components/BookListCompleted'
import BookListUncompleted from '../components/BookListUncompleted'

function BookShelf() {
  const {
    books,
    setCurrentBook,
    setBooks
  } = useGlobalContext()

  let navigate = useNavigate()

  // Update isComplete property value, when uncompleted/complete button clicked
  const toggleIsComplete = (id, isComplete) => {
    setBooks(books => {
      return books.map(book => book.id === id ? 
        {
          ...book, // copy obj  
          isComplete: !isComplete, // update  isComplete property
        } 
        : 
        book
      )
    })
  }

  // CRUD operations (delete book, edit book)
  const deleteBook = (id, title) => {
    if(window.confirm(`Apakah Anda Yakin Ingin Menghapus Buku ${title} ?`)) {
      setBooks(books.filter(book => book.id !== id))
    }
    return
  }

  const editBook = (bookId, bookTitle, bookAuthor, bookYear, bookImage, bookIsComplete) => {
    navigate(`/editbook/${bookId}`)
    setCurrentBook({
      id: bookId, 
      title: bookTitle, 
      author: bookAuthor, 
      year: bookYear, 
      image: bookImage,
      isComplete: bookIsComplete
    })
  }

  return (
    <div className="BookList container">
      <BookListUncompleted 
        books={books} 
        toggleIsComplete={toggleIsComplete}
        deleteBook={deleteBook}
        editBook={editBook}
      />
      <BookListCompleted 
        books={books} 
        toggleIsComplete={toggleIsComplete}
        deleteBook={deleteBook}
        editBook={editBook}
      />
    </div>
  )
}

export default BookShelf