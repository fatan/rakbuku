// Components
import Book from './Book'

function BookListCompleted(props) {
  return (
    <div className="BookList-container">
      <h2 className='heading'>Selesai Dibaca</h2> 
      <ul id="completed" className="BookList">
        {props.books.map(book => {
          return (
            book.isComplete &&
              <Book 
                key={book.id}
                toggleIsComplete={props.toggleIsComplete}
                deleteBook={props.deleteBook}
                editBook={props.editBook}
                {...book}
              />
            )
          })
        }
      </ul>
    </div>
  )
}

export default BookListCompleted