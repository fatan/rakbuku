// Components
import Book from './Book'

function BookListUncompleted(props) {  
  return (
    <div className="BookList-container">
      <h2 className='heading'>Belum Selesai Dibaca</h2> 
      <ul id="uncompleted" className="BookList">
        {props.books.map(book => {
          return (
            !book.isComplete &&
              <Book 
                key={book.id}
                toggleIsComplete={props.toggleIsComplete}
                deleteBook={props.deleteBook}
                editBook={props.editBook}
                {...book}
              />
            )
          })
        }
      </ul>
    </div>
  )
}

export default BookListUncompleted