// Context
import { useGlobalContext } from '../context'
// Components
import LinkTo from './LinkTo'
import SubmitButton from './SubmitButton'

function AddBookForm() {
  const { 
    book, 
    checked,
    images,
    books,
    setBook,
    setChecked,
    setImages,
    setBooks,
    setDisplay 
  } = useGlobalContext()

  // handle inputs change
  const handleInputChange = (event) => {
    const name = event.target.name
    const value = event.target.value
    const checked = event.target.checked

    setBook({...book, [name]: value})
    setChecked(checked)
  }

  // handle image change
  const handleImageChange = (event) => {
    return event.target.files || event.target.files.length > 0 ? 
    setImages(URL.createObjectURL(event.target.files[0])) 
    : 
    setImages()
  }

  // handle submit 
  const handleSubmit = (event) => {
    event.preventDefault()

    addBook(book)
    setTimeout(() => {
      setBook({})
    }, 1000)
  }

  // CRUD operations (add book)
  const addBook = (book) => {
    let sameTitle = books.filter(previousBook => book.title.toLowerCase() === previousBook.title.toLowerCase())
    book.id = books.length + 1
    book.image = images
    book.isComplete = checked
    
    if(sameTitle.length > 0 ) {
      window.alert(`buku ${book.title} gagal ditambahkan karena sudah ada`)
    } else {
      setBooks([...books, book])
      setTimeout(() => { 
        setDisplay(true) 
      }, 1500)
    }
  }

  return (
    <form className="AddBookForm" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="inputForJudulBuku">Judul Buku</label>
        <input 
          type="text"
          id="titleBook"
          name="title"
          value={book.title || ""}
          onChange={handleInputChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="inputForPenulis">Penulis</label>
        <input 
          type="text"
          id="author"
          name="author"
          value={book.author || ""}
          onChange={handleInputChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="inputForTahun">Tahun</label>
        <input 
          type="number"
          id="year"
          name="year"
          value={book.year || ""}
          onChange={handleInputChange}
        />
      </div>
      <div className="form-group">
        <label htmlFor="imageUpload">Image Upload</label>
        <input 
          type="file"
          accept="image/*"
          value = {book.image}
          onChange={handleImageChange}
        />
      </div>
      <div className="form-checkbox">
        <label htmlFor="selesaiDibaca">Selesai dibaca</label>
        <input 
          type="checkbox" 
          id="completeCheckbox" 
          name="isComplete"
          checked={checked}
          onChange={handleInputChange}
        />
      </div>
      <div className="flex jcc">
        <SubmitButton 
          value="Submit" 
          className="home m-10"
        />
        <LinkTo 
          to="/" 
          innerText="Home" 
          className="cancel m-10"
        />
      </div>
    </form>
  )
}

export default AddBookForm