// Components
import EditBookForm from '../components/EditBookForm'

function EditBook() {
  return (
      <div className="form-container card shadow-dark container">
        <h2 className="heading">EDIT BUKU</h2>
        <EditBookForm />
      </div>
  )
}

export default EditBook