// Components
import AddBookForm from '../components/AddBookForm'

function AddBook() {
  return (
    <div className="form-container card shadow-dark container">
      <h2 className="heading">TAMBAH BUKU</h2>
      <AddBookForm /> 
    </div>
  )
}

export default AddBook