import React, { 
  createContext, 
  useContext, 
  useEffect, 
  useState 
} from "react";

const AppContext = createContext()

const AppProvider = ({ children }) => {
  // setting global state
  const [book, setBook] = useState({})
  const [checked, setChecked] = useState(false)
  const [images, setImages] = useState()
  const [currentBook, setCurrentBook] = useState({})
  const [books, setBooks] = useState(() => { 
    // books data 
    return JSON.parse(localStorage.getItem("books_data")) !== null ? 
    JSON.parse(localStorage.getItem("books_data")) 
    :
    []
  })
  const [display, setDisplay] = useState(false)

  useEffect(() => {
    localStorage.setItem('books_data', JSON.stringify(books))
    console.log(books)
    console.log(currentBook)

  }, [books, currentBook])

  return (
    <AppContext.Provider
      value={{
        book, 
        currentBook,
        books, 
        checked, 
        images,
        display,
        setBook, 
        setChecked, 
        setImages,
        setCurrentBook,
        setBooks,
        setDisplay
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export const useGlobalContext = () => {
  return useContext(AppContext)
}

export {AppContext, AppProvider}